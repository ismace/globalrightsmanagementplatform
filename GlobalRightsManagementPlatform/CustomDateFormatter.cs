using System;
using System.Globalization;
using Humanizer;

namespace GlobalRightsManagementPlatform
{
    public class CustomDateFormatter : ICustomDateFormatter
    {
        public DateTime? Convert(string dateString)
        {
            if (dateString == string.Empty)
                return null;

            DateTime date;

            var dateParts = dateString.Split(' ');

            var day = dateParts[0]
                .Replace("nd", "")
                .Replace("th", "")
                .Replace("rd", "")
                .Replace("st", "");

            var month = dateParts[1].Substring(0, 3);
            var year = dateParts[2];

            dateString = string.Format("{0} {1} {2}", day, month, year);

            DateTime.TryParseExact(dateString, "d MMM yyyy", new CultureInfo("en-us"), DateTimeStyles.AssumeLocal,
                out date);

            return date;
        }

        public string Convert(DateTime? date)
        {
            if (!date.HasValue)
                return string.Empty;

            return string.Format("{0} {1}", date.Value.Day.Ordinalize(), date.Value.ToString("MMM yyyy"));
        }
    }
}