namespace GlobalRightsManagementPlatform.Models
{
    public enum DistributionType
    {
        DigitalDownload,
        Streaming
    }
}