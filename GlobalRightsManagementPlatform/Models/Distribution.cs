﻿using System;

namespace GlobalRightsManagementPlatform.Models
{
    public class Distribution
    {
        public DistributionType DistributionType { get; set; }
        public DateTime? StartDate;
        public DateTime? EndDate;

        public Distribution(DistributionType distributionType)
        {
            DistributionType = distributionType;
        }
    }
}