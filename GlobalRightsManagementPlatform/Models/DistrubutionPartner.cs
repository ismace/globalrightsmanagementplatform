﻿namespace GlobalRightsManagementPlatform.Models
{
    public class DistrubutionPartner
    {
        public DistrubutionPartner(string name)
        {
            Name = name;
        }

        public string Name { get; set; }
        public DistributionType DistributionType { get; set; }
    }
}