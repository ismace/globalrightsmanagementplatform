﻿using System.Collections.Generic;

namespace GlobalRightsManagementPlatform.Models
{
    public class Song
    {
        public string SongName { get; set; }
        public List<Distribution> Distributions { get; set; }

        public Song(string songName)
        {
            SongName = songName;
            Distributions = new List<Distribution>();
        }
    }
}