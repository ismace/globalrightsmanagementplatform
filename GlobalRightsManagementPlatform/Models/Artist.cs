﻿using System.Collections.Generic;

namespace GlobalRightsManagementPlatform.Models
{
    public class Artist
    {
        public Artist(string name)
        {
            Name = name;
            Songs = new List<Song>();
        }

        public string Name { get; set; }
        public List<Song> Songs { get; set; }
    }
}