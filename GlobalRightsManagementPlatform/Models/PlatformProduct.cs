using System;

namespace GlobalRightsManagementPlatform.Models
{
    public class PlatformProduct
    {
        public PlatformProduct(string artist, string songName, DistributionType distributionType, DateTime? starDate, DateTime? endDate)
        {
            var type = string.Empty;

            if (distributionType == Models.DistributionType.DigitalDownload)
                type = "digital download";

            else if (distributionType == Models.DistributionType.Streaming)
                type = "streaming";

            Artist = artist;
            SongName = songName;
            this.DistributionType = type;
            StartDate = starDate;
            EndDate = endDate;
        }

        public string Artist { get; set; }
        public string SongName { get; set; }
        public string DistributionType { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}