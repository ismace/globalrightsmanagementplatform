using System.Collections.Generic;
using System.Text;
using GlobalRightsManagementPlatform.Models;

namespace GlobalRightsManagementPlatform
{
    public class OutputParser
    {
        private readonly ICustomDateFormatter _customDateFormatter;

        public OutputParser(ICustomDateFormatter customDateFormatter)
        {
            _customDateFormatter = customDateFormatter;
        }

        public string Parse(List<PlatformProduct> platformProduct)
        {
            var output = new StringBuilder();

            output.Append("Artist")
                .Append("|")
                .Append("Title")
                .Append("|")
                .Append("Usage")
                .Append("|")
                .Append("StartDate")
                .Append("|")
                .Append("EndDate")
                .Append(System.Environment.NewLine);

            foreach (var product in platformProduct)
            {
                output.Append(product.Artist)
                    .Append("|")
                    .Append(product.SongName)
                    .Append("|")
                    .Append(product.DistributionType)
                    .Append("|")
                    .Append(_customDateFormatter.Convert(product.StartDate))
                    .Append("|")
                    .Append(_customDateFormatter.Convert(product.EndDate))
                    .Append(System.Environment.NewLine);
            }

            return output.ToString();
        }
    }
}