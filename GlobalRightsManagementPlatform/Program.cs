﻿using System;
using System.Collections.Generic;
using System.IO;
using GlobalRightsManagementPlatform.Models;

namespace GlobalRightsManagementPlatform
{
    internal class Program
    {
        /// <summary>
        /// As this is a simple console application objects were created manually.
        /// In a modern web application an IoC controller would handle this instead
        /// </summary>
        /// <param name="args"></param>
        private static void Main(string[] args)
        {
            var platformContext = new PlatformContext();

            if (!LoadMusicContracts(platformContext))
                ShowError();

            if (!LoadPartners(platformContext))
                ShowError();

            var availableProducts = QueryAvailableProducts(platformContext);

            Console.Write(new OutputParser(new CustomDateFormatter()).Parse(availableProducts));

            Console.WriteLine("Press any key to quit.");
            Console.ReadKey();
        }

        private static void ShowError()
        {
            Console.WriteLine("There has been a problem with your input. Press any key to quit.");
            Console.ReadKey();
            Environment.Exit(-1);
        }

        private static bool LoadMusicContracts(PlatformContext platformContext)
        {
            Console.WriteLine("Enter the full path to the file name for music contracts");
            var contractsFileName = Console.ReadLine();

            if (!File.Exists(contractsFileName))
                return false;

            var contracts = new InputParser().Parse(File.ReadAllText(contractsFileName));

            using (var unitOfWork = new UnitOfWork(platformContext))
            {
                unitOfWork.PlatformRepository.AddContracts(contracts);
                return true;
            }
        }

        private static bool LoadPartners(PlatformContext platformContext)
        {
            Console.WriteLine("Enter the full path to the file name for distribution partners");
            var partnersFileName = Console.ReadLine();

            if (!File.Exists(partnersFileName))
                return false;

            var partners = new InputParser().Parse(File.ReadAllText(partnersFileName));

            using (var unitOfWork = new UnitOfWork(platformContext))
            {
                unitOfWork.PlatformRepository.AddPartners(partners);
                return true;
            }
        }

        private static List<PlatformProduct> QueryAvailableProducts(PlatformContext platformContext)
        {
            Console.WriteLine("Enter provider and date to query available products");
            var query = Console.ReadLine();
            var partnerName = query.Split(' ')[0];
            var date = query.Substring(partnerName.Length + 1);

            using (var unitOfWork = new UnitOfWork(platformContext))
            {
                var partner = unitOfWork.PlatformRepository.GetPartner(partnerName);
                return unitOfWork.PlatformRepository.GetProducts(partner,
                    ((ICustomDateFormatter)new CustomDateFormatter()).Convert(date).Value);
            }
        }
    }
}