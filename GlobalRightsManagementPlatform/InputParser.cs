using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.VisualBasic.FileIO;

namespace GlobalRightsManagementPlatform
{
    public class InputParser : IInputParser
    {
        public IEnumerable<List<string>> Parse(string input)
        {
            var result = new List<List<string>>();

            using (var stream = new MemoryStream())
            {
                var bytes = Encoding.Default.GetBytes(input);
                stream.Write(bytes, 0, bytes.Length);
                stream.Seek(0, SeekOrigin.Begin);

                using (var parser = new TextFieldParser(stream))
                {
                    parser.Delimiters = new[] {"|"};

                    var firstLine = true;

                    while (true)
                    {
                        if (firstLine)
                        {
                            parser.ReadFields();
                            firstLine = false;
                            continue;
                        }

                        var parts = parser.ReadFields();

                        if (parts == null)
                            break;

                        result.Add(parts.ToList());
                    }
                }
            }

            return result;
        }
    }
}