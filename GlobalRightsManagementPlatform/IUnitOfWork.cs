﻿using System;

namespace GlobalRightsManagementPlatform
{
    public interface IUnitOfWork : IDisposable
    {
        IPlatformRepository PlatformRepository { get; }
    }
}