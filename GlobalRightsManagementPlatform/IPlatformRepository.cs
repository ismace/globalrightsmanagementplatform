﻿using System;
using System.Collections.Generic;
using GlobalRightsManagementPlatform.Models;

namespace GlobalRightsManagementPlatform
{
    public interface IPlatformRepository
    {
        void AddContracts(IEnumerable<List<string>> contracts);
        void AddPartners(IEnumerable<List<string>> partners);
        List<PlatformProduct> GetProducts(DistrubutionPartner partner, DateTime date);
        DistrubutionPartner GetPartner(string partnerName);
    }
}