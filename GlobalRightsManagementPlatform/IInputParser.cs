using System.Collections.Generic;

namespace GlobalRightsManagementPlatform
{
    public interface IInputParser
    {
        IEnumerable<List<string>> Parse(string input);
    }
}