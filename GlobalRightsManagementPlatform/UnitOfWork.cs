﻿using System;

namespace GlobalRightsManagementPlatform
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly PlatformContext _platformContext;
        public IPlatformRepository PlatformRepository { get; private set; }

        public UnitOfWork(PlatformContext platformContext)
        {
            _platformContext = platformContext;
            PlatformRepository = new PlatformRepository(platformContext, new CustomDateFormatter());
        }

        public void Dispose()
        {
            _platformContext.Dispose();
        }
    }
}