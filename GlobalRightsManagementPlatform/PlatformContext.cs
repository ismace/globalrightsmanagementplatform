using System.Collections.Generic;
using GlobalRightsManagementPlatform.Models;

namespace GlobalRightsManagementPlatform
{
    public class PlatformContext
    {
        public List<Artist> Artists { get; set; }
        public List<DistrubutionPartner> DistrubutionPartners { get; set; }

        public void Dispose()
        {            
        }
    }
}