using System;

namespace GlobalRightsManagementPlatform
{
    public interface ICustomDateFormatter
    {
        DateTime? Convert(string dateString);
        string Convert(DateTime? date);
    }
}