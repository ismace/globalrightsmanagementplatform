﻿using System;
using System.Collections.Generic;
using System.Linq;
using GlobalRightsManagementPlatform.Models;

namespace GlobalRightsManagementPlatform
{
    public class PlatformRepository : IPlatformRepository
    {
        private readonly ICustomDateFormatter _customDateFormatter;
        private readonly PlatformContext _platformContext;

        public PlatformRepository(PlatformContext platformContext,
            ICustomDateFormatter customDateFormatter)
        {
            _customDateFormatter = customDateFormatter;
            _platformContext = platformContext;
        }

        public void AddContracts(IEnumerable<List<string>> contracts)
        {
            _platformContext.Artists = new List<Artist>();

            foreach (var contractPart in contracts)
            {
                var artistName = contractPart[0];

                Artist artist;

                if (!_platformContext.Artists.Select(a => a.Name).Contains(artistName))
                {
                    artist = new Artist(artistName);
                    _platformContext.Artists.Add(artist);
                }
                else
                {
                    artist = _platformContext.Artists.First(a => a.Name.Equals(artistName));
                }

                var song = new Song(contractPart[1]);

                foreach (var type in contractPart[2].Split(','))
                {
                    song.Distributions.Add(new Distribution(ResolveDistributionType(type))
                    {
                        StartDate = _customDateFormatter.Convert(contractPart[3]),
                        EndDate = _customDateFormatter.Convert(contractPart[4])
                    });
                }

                artist.Songs.Add(song);
            }
        }

        public void AddPartners(IEnumerable<List<string>> partners)
        {
            _platformContext.DistrubutionPartners = new List<DistrubutionPartner>();

            foreach (var partnerPart in partners)
            {
                var partner = new DistrubutionPartner(partnerPart[0]);

                foreach (var type in partnerPart[1].Split(','))
                    partner.DistributionType = ResolveDistributionType(type);

                _platformContext.DistrubutionPartners.Add(partner);
            }
        }

        public List<PlatformProduct> GetProducts(DistrubutionPartner partner, DateTime date)
        {
            var products = new List<PlatformProduct>();

            foreach (var artist in _platformContext.Artists)
            {
                foreach (var song in artist.Songs)
                {
                    foreach (var distribution in song.Distributions)
                    {
                        if (distribution.DistributionType == partner.DistributionType &&
                            date > distribution.StartDate.Value)
                        {
                            if (!distribution.EndDate.HasValue ||
                                (distribution.EndDate.HasValue && date < distribution.EndDate.Value))
                            {
                                products.Add(new PlatformProduct(artist.Name, song.SongName,
                                    distribution.DistributionType, distribution.StartDate, distribution.EndDate));
                            }
                        }
                    }
                }
            }
            return products.OrderBy(p => p.Artist).ThenBy(p => p.SongName).ToList();
        }

        public Artist GetArtist(string name)
        {
            return _platformContext.Artists.FirstOrDefault(d => d.Name.Equals(name));
        }

        public DistrubutionPartner GetPartner(string partnerName)
        {
            return _platformContext.DistrubutionPartners.FirstOrDefault(d => d.Name.Equals(partnerName));
        }

        private static DistributionType ResolveDistributionType(string type)
        {
            if (type.Trim().Equals("digital download", StringComparison.InvariantCultureIgnoreCase))
            {
                return DistributionType.DigitalDownload;
            }

            if (type.Trim().Equals("streaming", StringComparison.InvariantCultureIgnoreCase))
            {
                return DistributionType.Streaming;
            }

            throw new InvalidOperationException();
        }

        public DistrubutionPartner GetDistributionPartnerByName(string name)
        {
            return _platformContext.DistrubutionPartners.FirstOrDefault(d => d.Name.Equals(name));
        }
    }
}