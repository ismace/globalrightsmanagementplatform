﻿using System;
using System.Collections.Generic;
using GlobalRightsManagementPlatform;
using GlobalRightsManagementPlatform.Models;
using NUnit.Framework;

namespace UnitTests
{
    public class OutputParserTests
    {
        [Test]
        public void ShouldPopulateOutput()
        {
            // Assign
            var products = new List<PlatformProduct>();
            var product1 = new PlatformProduct("Michael", "Bad", DistributionType.Streaming, new DateTime(2011, 3, 1), null);
            var product2 = new PlatformProduct("John", "Good", DistributionType.DigitalDownload, new DateTime(2012, 2, 1), null);
            products.Add(product1);
            products.Add(product2);

            // Act
            var output = new OutputParser(new CustomDateFormatter()).Parse(products);

            // Assert
            Assert.AreEqual(_expectedOutput, output );
        }

        private readonly string _expectedOutput = "Artist|Title|Usage|StartDate|EndDate" + Environment.NewLine +
            "Michael|Bad|streaming|1st Mar 2011|" + Environment.NewLine +
            "John|Good|digital download|1st Feb 2012|" + Environment.NewLine;
    }
}
