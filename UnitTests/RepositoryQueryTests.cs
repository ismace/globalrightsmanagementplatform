﻿using System;
using System.Text;
using GlobalRightsManagementPlatform;
using NUnit.Framework;

namespace UnitTests
{
    [TestFixture]
    public class RepositoryQueryTests
    {
        private static string LoadSampleContracts()
        {
            var sampleContracts = new StringBuilder();
            sampleContracts.Append("Artist|Title|Usages|StartDate|EndDate\r\n");
            sampleContracts.Append("Tinie Tempah|Frisky (Live from SoHo)|digital download|1st Feb 2012|1st Feb 2014\r\n");

            return sampleContracts.ToString();
        }

        private static string LoadSamplePartners()
        {
            var sampleContracts = new StringBuilder();
            sampleContracts.Append("Partner|Usage\r\n");
            sampleContracts.Append("ITunes|digital download\r\n");
            sampleContracts.Append("YouTube|streaming\r\n");

            return sampleContracts.ToString();
        }

        private static PlatformRepository LoadRepository()
        {
            var repository = new PlatformRepository(new PlatformContext(), new CustomDateFormatter());
            var sampleContracts = LoadSampleContracts();
            var partners = LoadSamplePartners();
            var inputParser = new InputParser();
            repository.AddContracts(inputParser.Parse(sampleContracts));
            repository.AddPartners(inputParser.Parse(partners));
            return repository;
        }

        [Test]
        public void ShouldReturnArtist()
        {
            // Assign
            var repository = LoadRepository();

            // Act
            var platformProducts = repository.GetProducts(repository.GetDistributionPartnerByName("ITunes"), new DateTime(2012, 3, 1));

            // Assert
            Assert.AreEqual("Tinie Tempah", platformProducts[0].Artist);
        }

        [Test]
        public void ShouldReturnTitle()
        {
            // Assign
            var repository = LoadRepository();

            // Act
            var platformProducts = repository.GetProducts(repository.GetDistributionPartnerByName("ITunes"), new DateTime(2012, 3, 1));

            // Assert
            Assert.AreEqual("Frisky (Live from SoHo)", platformProducts[0].SongName);
        }

        [Test]
        public void ShouldReturnDistributionTypes()
        {
            // Assign
            var repository = LoadRepository();

            // Act
            var platformProducts = repository.GetProducts(repository.GetDistributionPartnerByName("ITunes"), new DateTime(2012, 3, 1));

            // Assert
            Assert.AreEqual("digital download", platformProducts[0].DistributionType);
        }

        [Test]
        public void ShouldReturnDistributionStartDate()
        {
            // Assign
            var repository = LoadRepository();

            // Act
            var platformProducts = repository.GetProducts(repository.GetDistributionPartnerByName("ITunes"), new DateTime(2012, 3, 1));

            // Assert
            Assert.True(DateTime.Compare(new DateTime(2012, 2, 1), platformProducts[0].StartDate.Value) == 0);
        }

        [Test]
        public void ShouldReturnDistributionEndDate()
        {
            // Assign
            var repository = LoadRepository();

            // Act
            var platformProducts = repository.GetProducts(repository.GetDistributionPartnerByName("ITunes"), new DateTime(2012, 3, 1));

            // Assert
            Assert.True(DateTime.Compare(new DateTime(2014, 2, 1), platformProducts[0].EndDate.Value) == 0);
        }

        [Test]
        public void ShouldNotReturnProductsBeforeStartDate()
        {
            // Assign
            var repository = LoadRepository();

            // Act
            var platformProducts = repository.GetProducts(repository.GetDistributionPartnerByName("ITunes"), new DateTime(2011, 3, 1));

            // Assert
            Assert.IsEmpty(platformProducts);
        }

        [Test]
        public void ShouldNotReturnProductsAfterEndDate()
        {
            // Assign
            var repository = LoadRepository();

            // Act
            var platformProducts = repository.GetProducts(repository.GetDistributionPartnerByName("ITunes"), new DateTime(2015, 3, 1));

            // Assert
            Assert.IsEmpty(platformProducts);
        }

        [Test]
        public void ShouldNotReturnProductsOffThePartner()
        {
            // Assign
            var repository = LoadRepository();

            // Act
            var platformProducts = repository.GetProducts(repository.GetDistributionPartnerByName("YouTube"), new DateTime(2013, 3, 1));

            // Assert
            Assert.IsEmpty(platformProducts);
        }

        private static string LoadSampleContractsWithoutEndDate()
        {
            var sampleContracts = new StringBuilder();
            sampleContracts.Append("Artist|Title|Usages|StartDate|EndDate\r\n");
            sampleContracts.Append("Tinie Tempah|Frisky (Live from SoHo)|digital download|1st Feb 2012|\r\n");

            return sampleContracts.ToString();
        }

        private PlatformRepository LoadRepositoryWithoutEndDate()
        {
            var repository = new PlatformRepository(new PlatformContext(), new CustomDateFormatter());
            var sampleContracts = LoadSampleContractsWithoutEndDate();
            var partners = LoadSamplePartners();
            var inputParser = new InputParser();
            repository.AddContracts(inputParser.Parse(sampleContracts));
            repository.AddPartners(inputParser.Parse(partners));
            return repository;
        }

        [Test]
        public void ShouldReturnProductsWhenThereIsNoDistributionEndDate()
        {
            // Assign
            var repository = LoadRepositoryWithoutEndDate();

            // Act
            var platformProducts = repository.GetProducts(repository.GetDistributionPartnerByName("ITunes"), new DateTime(2013, 3, 1));

            // Assert
            Assert.IsNotEmpty(platformProducts);
        }
    }
}
