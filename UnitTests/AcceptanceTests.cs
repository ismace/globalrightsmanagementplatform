﻿using System;
using System.Text;
using GlobalRightsManagementPlatform;
using NUnit.Framework;

namespace UnitTests
{
    /// <summary>
    /// For the sake of splicity acceptance test are implemented as a class rather than a new project in the solution
    /// </summary>
    public class AcceptanceTests
    {
        private static string LoadSampleContracts()
        {
            var sampleContracts = new StringBuilder();
            sampleContracts.Append("Artist|Title|Usages|StartDate|EndDate\r\n");
            sampleContracts.Append("Tinie Tempah|Frisky (Live from SoHo)|digital download, streaming|1st Feb 2012|\r\n");
            sampleContracts.Append("Tinie Tempah|Miami 2 Ibiza|digital download|1st Feb 2012|\r\n");
            sampleContracts.Append("Tinie Tempah|Till I'm Gone|digital download|1st Aug 2012|\r\n");
            sampleContracts.Append("Monkey Claw|Black Mountain|digital download|1st Feb 2012|\r\n");
            sampleContracts.Append("Monkey Claw|Iron Horse|digital download, streaming|1st June 2012|\r\n");
            sampleContracts.Append("Monkey Claw|Motor Mouth|digital download, streaming|1st Mar 2011|\r\n");
            sampleContracts.Append("Monkey Claw|Christmas Special|streaming|25st Dec 2012|31st Dec 2012\r\n");

            return sampleContracts.ToString();
        }

        private static string LoadSamplePartners()
        {
            var sampleContracts = new StringBuilder();
            sampleContracts.Append("Partner|Usage\r\n");
            sampleContracts.Append("ITunes|digital download\r\n");
            sampleContracts.Append("YouTube|streaming\r\n");

            return sampleContracts.ToString();
        }

        [Test]
        public void Scenario1()
        {
            // Assing
            var repository = new PlatformRepository(new PlatformContext(), new CustomDateFormatter());
            var sampleContracts = LoadSampleContracts();
            var partners = LoadSamplePartners();
            var inputParser = new InputParser();
            repository.AddContracts(inputParser.Parse(sampleContracts));
            repository.AddPartners(inputParser.Parse(partners));

            // Act
            var platformProducts = repository.GetProducts(repository.GetDistributionPartnerByName("ITunes"), new DateTime(2012, 3, 1));

            // Assert
            var output = new OutputParser(new CustomDateFormatter()).Parse(platformProducts);

            var expectedOutput = "Artist|Title|Usage|StartDate|EndDate" + Environment.NewLine +
                                 "Monkey Claw|Black Mountain|digital download|1st Feb 2012|" + Environment.NewLine +
                                 "Monkey Claw|Motor Mouth|digital download|1st Mar 2011|" + Environment.NewLine +
                                 "Tinie Tempah|Frisky (Live from SoHo)|digital download|1st Feb 2012|" + Environment.NewLine +
                                 "Tinie Tempah|Miami 2 Ibiza|digital download|1st Feb 2012|" + Environment.NewLine;

            Assert.AreEqual(expectedOutput, output);
        }

        [Test]
        public void Scenario2()
        {
            // Assing
            var repository = new PlatformRepository(new PlatformContext(), new CustomDateFormatter());
            var sampleContracts = LoadSampleContracts();
            var partners = LoadSamplePartners();
            var inputParser = new InputParser();
            repository.AddContracts(inputParser.Parse(sampleContracts));
            repository.AddPartners(inputParser.Parse(partners));

            // Act
            var platformProducts = repository.GetProducts(repository.GetDistributionPartnerByName("YouTube"), new DateTime(2012, 4, 1));

            // Assert
            var output = new OutputParser(new CustomDateFormatter()).Parse(platformProducts);

            var expectedOutput = "Artist|Title|Usage|StartDate|EndDate" + Environment.NewLine +
                                 "Monkey Claw|Motor Mouth|streaming|1st Mar 2011|" + Environment.NewLine +
                                 "Tinie Tempah|Frisky (Live from SoHo)|streaming|1st Feb 2012|" + Environment.NewLine;

            Assert.AreEqual(expectedOutput, output);
        }

        [Test]
        public void Scenario3()
        {
            // Assing
            var repository = new PlatformRepository(new PlatformContext(), new CustomDateFormatter());
            var sampleContracts = LoadSampleContracts();
            var partners = LoadSamplePartners();
            var inputParser = new InputParser();

            repository.AddContracts(inputParser.Parse(sampleContracts));
            repository.AddPartners(inputParser.Parse(partners));

            // Act
            var platformProducts = repository.GetProducts(repository.GetDistributionPartnerByName("YouTube"), new DateTime(2012, 12, 27));

            // Assert
            var output = new OutputParser(new CustomDateFormatter()).Parse(platformProducts);

            var expectedOutput = "Artist|Title|Usage|StartDate|EndDate" + Environment.NewLine +
                                 "Monkey Claw|Christmas Special|streaming|25th Dec 2012|31st Dec 2012" + Environment.NewLine +
                                 "Monkey Claw|Iron Horse|streaming|1st Jun 2012|" + Environment.NewLine +
                                 "Monkey Claw|Motor Mouth|streaming|1st Mar 2011|" + Environment.NewLine +
                                 "Tinie Tempah|Frisky (Live from SoHo)|streaming|1st Feb 2012|" + Environment.NewLine;

            Assert.AreEqual(expectedOutput, output);
        }
    }
}
