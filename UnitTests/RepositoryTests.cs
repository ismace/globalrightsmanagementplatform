﻿using System;
using System.Linq;
using System.Text;
using GlobalRightsManagementPlatform;
using GlobalRightsManagementPlatform.Models;
using NUnit.Framework;

namespace UnitTests
{
    [TestFixture]
    public class RepositoryTests
    {
        private static string LoadSampleContracts()
        {
            var sampleContracts = new StringBuilder();
            sampleContracts.Append("Artist|Title|Usages|StartDate|EndDate\r\n");
            sampleContracts.Append("Tinie Tempah|Frisky (Live from SoHo)|digital download, streaming|1st Feb 2012|1st Feb 2014|\r\n");
            sampleContracts.Append("Monkey Claw|Christmas Special|streaming|25st Dec 2012|31st Dec 2012|\r\n");

            return sampleContracts.ToString();
        }

        [Test]
        public void ShouldLoadArtistsSongs()
        {
            // Assign
            var repository = new PlatformRepository(new PlatformContext(), new CustomDateFormatter());
            var sampleContracts = LoadSampleContracts();

            // Act
            repository.AddContracts(new InputParser().Parse(sampleContracts));

            // Assert
            var artist = repository.GetArtist("Tinie Tempah");
            Assert.AreEqual("Frisky (Live from SoHo)", artist
                .Songs.First().SongName);
        }

        [Test]
        public void ShouldLoadArtistsSongsDistributionType()
        {
            // Assign
            var repository = new PlatformRepository(new PlatformContext(), new CustomDateFormatter());
            var sampleContracts = LoadSampleContracts();

            // Act
            repository.AddContracts(new InputParser().Parse(sampleContracts));

            // Assert
            var song = repository.GetArtist("Tinie Tempah").Songs.First();
            Assert.AreEqual(2, song.Distributions.Count);
            Assert.True(song.Distributions.Select(d => d.DistributionType).Contains(DistributionType.DigitalDownload));
            Assert.True(song.Distributions.Select(d => d.DistributionType).Contains(DistributionType.Streaming));
        }

        [Test]
        public void ShouldLoadArtistsSongsDistributionStartDate()
        {
            // Assign
            var repository = new PlatformRepository(new PlatformContext(), new CustomDateFormatter());
            var sampleContracts = LoadSampleContracts();

            // Act
            repository.AddContracts(new InputParser().Parse(sampleContracts));

            // Assert
            var distributions = repository.GetArtist("Tinie Tempah").Songs.First().Distributions;
            var distribution = distributions.First(d => d.DistributionType.Equals(DistributionType.DigitalDownload));
            Assert.True(DateTime.Compare(new DateTime(2012, 2, 1), distribution.StartDate.Value) == 0);
            distribution = distributions.First(d => d.DistributionType.Equals(DistributionType.Streaming));
            Assert.True(DateTime.Compare(new DateTime(2012, 2, 1), distribution.StartDate.Value) == 0);
        }

        [Test]
        public void ShouldLoadArtistsSongsDistributionEndDate()
        {
            // Assign
            var repository = new PlatformRepository(new PlatformContext(), new CustomDateFormatter());
            var sampleContracts = LoadSampleContracts();

            // Act
            repository.AddContracts(new InputParser().Parse(sampleContracts));

            // Assert

            var distribution = repository.GetArtist("Monkey Claw").Songs.First().Distributions.First();
            Assert.True(DateTime.Compare(new DateTime(2012, 12, 31), distribution.EndDate.Value) == 0);
        }

        private static string LoadSamplePartners()
        {
            var sampleContracts = new StringBuilder();
            sampleContracts.Append("Partner|Usage\r\n");
            sampleContracts.Append("ITunes|digital download\r\n");
            sampleContracts.Append("YouTube|streaming\r\n");

            return sampleContracts.ToString();
        }

        [Test]
        public void ShouldLoadPartnersDistributionType()
        {
            // Assign
            var repository = new PlatformRepository(new PlatformContext(), new CustomDateFormatter());
            var partners = LoadSamplePartners();

            // Act
            repository.AddPartners(new InputParser().Parse(partners));

            // Assert
            Assert.AreEqual(DistributionType.DigitalDownload, repository.GetDistributionPartnerByName("ITunes").DistributionType);
            Assert.AreEqual(DistributionType.Streaming, repository.GetDistributionPartnerByName("YouTube").DistributionType);
        }
    }
}